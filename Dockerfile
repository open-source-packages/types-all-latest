# syntax = docker/dockerfile:1.4

FROM python:3.11-slim-bullseye
WORKDIR /app
COPY . .
RUN rm -f /etc/apt/apt.conf.d/docker-clean; echo 'Binary::apt::APT::Keep-Downloaded-Packages "true";' > /etc/apt/apt.conf.d/keep-cache
RUN --mount=type=cache,target=/var/cache/apt,sharing=locked \
    --mount=type=cache,target=/var/lib/apt,sharing=locked \
    --mount=type=cache,target=/root/.cache/pip \
    apt update && apt-get --no-install-recommends install -y \
    git \
    && pip install --upgrade pip \
    && pip install -r requirements.txt \
    && pre-commit install --install-hooks \
    && rm -rf /var/lib/apt/lists/* \
    && python3 -m pip install --upgrade build \
    && python3 -m bin.generate-install-requires || true \
    && python3 -m build
RUN python3 -m pip install --upgrade twine
